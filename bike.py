from vertex import Vertex, Wheel
from road import Road
import numpy as np
import itertools
from springs import Spring
from matplotlib.patches import Circle
from matplotlib.lines import Line2D


class Bike:
    """
    Class that represents a bike a collection of 4 vertices and 6 springs. 
    Each vertex represents a rigid body and all the bodies are full mesh connected by the 6 springs.
    Two of the bodies are wheels, with a rotational constant acceleration. The other two bodies represent
    the load of the vehicle, and does not have rotational acceleration.
    """

    def __init__(self, name, vertices, springs, spring_dict):
        """
        Create a new bike

        :param name: The unique bike identifier
        :param vertices: Wheels/loads the bike needs to be initialized with
        :param springs: Springs that connect the vertices
        :param spring_dict: How the springs and vertices are linked
        """
        self.vertices = vertices
        self.springs = springs
        self.name = name
        self.is_finished = False
        self.distance_travelled = 0.0
        self.spring_dict = spring_dict
        self.anim_dict = {}
        self._create_animation_objects()

    def _create_animation_objects(self):
        """
        Create animation objects that are used to represent the wheels and springs in the visualisation

        :return: None
        """
        for vertex in self.vertices:
            self.anim_dict[vertex] = Circle(tuple(vertex.position), vertex.radius)
        for v1, v2 in self.spring_dict:
            spring = self.spring_dict[(v1, v2)]
            self.anim_dict[spring] = Line2D((v1.position[0], v2.position[0]), (v1.position[1], v2.position[1]),
                                            color='r')

    def get_patches(self):
        """
        Returns a view with the animation objects of the vertices and springs.

        :return:
        """
        return list(self.anim_dict.values()).copy()

    @staticmethod
    def get_random_bike(num_vertices=4):
        """
        Create a bike with random parameter values for the vertices and springs

        :return: Bike
        """
        road = Road.get_flat_road(10)
        vertex_list = []
        for i in range(num_vertices):
            position = np.random.random(2) + 1
            if i == 0:
                vertex_list.append(
                    Wheel(rotational_acceleration=1, mass=1, radius=0.05, position=position, velocity=[0, 0],
                          acceleration=[0, 1],
                          road=road))
            elif i == 1:
                vertex_list.append(
                    Wheel(rotational_acceleration=0, mass=1, radius=0.05, position=position, velocity=[0, 0],
                          acceleration=[0, 1],
                          road=road))
            else:
                vertex_list.append(Vertex(mass=1, radius=0.05, position=position, velocity=[0, 0], acceleration=[0, 0],
                                          road=road))

        spring_list = []
        spring_dict = {}
        for vertex1, vertex2 in itertools.combinations(vertex_list, 2):
            spring = Spring(elastic_constant=1000, vertex1=vertex1, vertex2=vertex2)
            spring_dict[(vertex1, vertex2)] = spring
            spring_list.append(spring)
        name = ("%.3f" % np.random.random())[2:6]
        return Bike(name, vertex_list, spring_list, spring_dict)

    def update_bike(self, dt):
        """
        Update the position of the bike with `dt` seconds

        :param dt: Time step size (s)
        :return: None
        """
        touched_ground = False
        for vertex in self.vertices:
            vertex.update_vertex(dt)
            self.anim_dict[vertex].center = tuple(vertex.position)
            for v1, v2 in self.spring_dict:
                spring = self.spring_dict[(v1, v2)]
                self.anim_dict[spring].set_data((v1.position[0], v2.position[0]), (v1.position[1], v2.position[1]))
            touched_ground |= (vertex.road_touch() and not isinstance(vertex, Wheel))
        self.distance_travelled = self.vertices[0].position[0]
        self.is_finished = touched_ground

    def __str__(self):
        vertex_string = "\n".join(str(vertex) for vertex in self.vertices)
        spring_string = "\n".join(str(spring) for spring in self.springs)
        return "Bike %s: Distance travelled: %.2f\nVertices: %s\nSprings: %s" % \
               (self.name, self.distance_travelled, vertex_string, spring_string)
