import numpy as np


class Road:
    """
    Class that represents the road.
    A 2D road is modelled as a sequence of points, connected with line segments.
    """

    def __init__(self, length, flat=True):
        """
        Create a new road object of given length.

        :param length: Horizontal length of the road
        :param flat: Whether the road is flat or has random in- and declinations
        """
        self.length = length
        self.road_points = []
        if flat:
            self._make_road_flat()
        else:
            self._make_road_random()

    def get_height_at(self, x):
        """
        Obtain the height of the road for a given x-coordinate.

        :param x: x-coordinate of road
        :return: y-coordinate as a float
        """
        return 0

    def _make_road_flat(self):
        """
        Flatten the road object

        :return: None
        """
        inclines = np.vstack((np.arange(0, self.length), np.zeros(self.length)))
        self.road_points = inclines

    def _make_road_random(self):
        """
        Create random inclinations and declinations

        :return: None
        """
        inclines = np.random.random([2, 10])
        inclines[0, :] = np.cumsum(inclines[0, :], axis=1)
        self.road_points = inclines

    @staticmethod
    def get_flat_road(length):
        """
        Obtain a road which has no inclination (constant y coordinates).

        :param length: Length of the road
        :return: new Road object
        """
        return Road(length, flat=True)

    @staticmethod
    def get_random_road(length):
        """
        Obtain a random road with random inclinations.

        :param length: Length of the road
        :return: new Road object
        """
        return Road(length, flat=False)
