import matplotlib.pyplot as plt
import matplotlib.animation as animation
from bike import Bike
from matplotlib.patches import Circle
from matplotlib.lines import Line2D
from road import Road
from environment import Environment

class Visualization:
    """
    Class related to the visualisation of the environment and the bikes
    """

    def __init__(self, environment):
        """
        Create a new visualisation object.
        """
        self.environment = environment
        self.fig = plt.figure()
        self.ax = plt.axes(xlim=(0,self.environment.length),ylim=(0,self.environment.height))
        self.delay = 100
        self.animation = None
        self.dt = 0.001

    def run_generation(self):
        """
        Iterate over the environment until the environment is finished, i.e., no bikes are moving anymore.
        Each iteration, the animation is updated. This is the main method responsible for moving the simulation forward.

        :return: None
        """
        self.init()
        self.animation = animation.FuncAnimation(self.fig, self.animate, interval=self.delay, blit=False)
        plt.show()

    def init(self):
        all_bike_patches = []
        for bike in self.environment.bikes:
            all_bike_patches += bike.get_patches()
        patch_tuple = tuple(all_bike_patches)
        for patch in patch_tuple:
            if isinstance(patch, Circle):
                self.ax.add_patch(patch)  # Todo: Add the road here as well
            elif isinstance(patch, Line2D):
                self.ax.add_line(patch)
            else:
                raise ValueError("Patch %s not recognised" % patch)

        return patch_tuple

    def animate(self, frame_number):
        """
        Update the current animation with data from Environment object env.

        :return: None
        """
        self.environment.update_env(self.dt)
        all_bike_patches = []
        for bike in self.environment.bikes:
            all_bike_patches += bike.get_patches()
            
        patch_tuple = tuple(all_bike_patches)
        # self.ax.autoscale()
        # print(frame_number)
        
        xpos, ypos = patch_tuple[0].center

        self.ax.set_xlim(xpos - 5, xpos + 5)
        return patch_tuple


