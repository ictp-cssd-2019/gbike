.. GBike documentation master file, created by
   sphinx-quickstart on Tue May  7 17:16:24 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Genetic Bike documentation!
=================================

.. _main_page:

The Genetic Bike (GBike) is a simulation in Python to evolve an optimal bicycle frame by using genetic algorithms ::

 -------- __@      __@       __@       __@      __~@
 ----- _`\<,_    _`\<,_    _`\<,_     _`\<,_    _`\<,_
 ---- (*)/ (*)  (*)/ (*)  (*)/ (*)  (*)/ (*)  (*)/ (*) 
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  

Description
-----------

The GBike project has three purposes:

 - Implement the physics of how a bike drives on a road (see :ref:`physics`).
 - Visualize how a bike rides in a matplotlib animation (see :ref:`animation`).
 - Improve the bike design by applying an evolutionary algorithm (see :ref:`evolution`).

.. toctree::
    :maxdepth: 2
    :caption: Modules:

    physics
    animation
    evolution
    bike
    vertex
    wheel
    springs
    road
    evolver
    environment
    visualization


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
