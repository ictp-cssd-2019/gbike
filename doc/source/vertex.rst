Documentation for the Vertex module
=============================================

.. automodule:: vertex
   :members: 
   :private-members: _compute_total_spring_force, _neighbor_points 

.. automodule:: wheel
   :members:

