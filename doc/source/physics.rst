Modules
======================

.. _physics:

Physics
---------

This module is responsible for the physics behind the bike movement.
The vertices respect Newton's laws of motion and the springs follow mass-spring principles.
We use an explicit Euler scheme to integrate the movement in time.

-Numerical mechanics

For each point, we keep track of four numbers: the position and velocity in the two dimensions:

.. math::

	x_i, y_i, v_{x,i}, v_{y,i}

In the numerical solver, the positions are updated by adding the velocities:

.. math::

	x_i(t+\Delta t) = x_i(t) + v_{x,i} \Delta t

The velocities are updated similarly by adding the accelerations in the different directions:

.. math::

	v_{x,i}(t+\Delta t) = v_{x,i}(t) + a_{x,i} \Delta t	

The actual physical behaviour of the system is encoded in the calculation of the accelerations :math:`a_{x,i}` at each time step. Sum up all forces acting in that direction, and divide by the mass of the point:

.. math::

	a_{x,i}(t) = \frac{ \sum_j{F_{x,ij}} }{ m_i }



