This repository is used to develop a simulation and genetic optimizer of bicycles.

## Installation

Clone this repository with `git clone https://gitlab.com/ictp-cssd-2019/gbike.git`.

This repository relies on:

 - Python 3
 - NumPy
 - Matplotlib

All these packages can be installed from `pip`/`conda`.

## Usage

To run this package, do 

```bash
cd gbike
python3 manager.py
```

## Structure

Basical modules

 - Visualization
 - Physical modelling
 - Iteration

An intermediate UML class diagram is present here:

![Example image](/uml_diagram.png/?raw=true "UML class diagram")

## Documentation

Documentation can be found [here](https://ictp-cssd-2019.gitlab.io/gbike/)

If desired, documentation pages can be generated and servered locally using `sphinx` in a variety of output formats.

In order to generate a website, install `sphinx` and run:

```bash
cd doc
make html
```
## License

This project is licensed under the MIT license. More details can be found in `license.md`.
