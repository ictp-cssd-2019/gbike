from bike import Bike
from road import Road
from vertex import Vertex, Wheel
from visualization import Visualization
from environment import Environment


class Manager:

    def __init__(self):
        height = 3
        length = 10
        self.environment = Environment(height, length)
        num_bikes = 2
        initial_bikes = [Bike.get_random_bike() for _ in range(num_bikes)]
        self.environment.reset_env(initial_bikes)
        self.visualization = Visualization(self.environment)
        self.environment.road = Road.get_flat_road(10)

    def run(self):
        self.visualization.run_generation()


if __name__ == '__main__':
    manager = Manager()
    manager.run()
