from bike import Bike
from road import Road

class Environment:
    """
    This class represents the environment in which the bikes travel the road
    The environment is a 2D box with fixed dimensions.
    The same environment will be used for different generations of bikes.
    Updating the environment will update everything that is present in it.
    """
    gravity = 9.81

    def __init__(self, height, length):
        """
        Create a new environment object. Only one is needed for the lifetime of the simulation.

        :param height: Height of the 2D box
        :param length: Length of the 2D box
        """
        self.time = 0
        self.height = height
        self.length = length
        self.bikes = []
        self.road = Road.get_flat_road(length)

    def reset_env(self, bikes):
        """
        Reset the environment with a new set of bikes at their starting point.

        :param bikes: List of bikes. If empty, new random bikes will be provided.
        :return: None
        """
        self.time = 0
        if bikes:
            self.bikes = bikes
        else:
            self.bikes = [Bike.get_random_bike() for _ in range(10)]

    def update_env(self, dt):
        """
        Update all the objects in the environment as time progresses

        :param dt: Time step size.
        :return: None
        """
        self.time += dt
        for bike in self.bikes:
            bike.update_bike(dt)

    def is_finished(self):
        """
        The environment is done (serves no longer a purpose) if all bikes have reached a stop.

        :return: True if all bikes have stopped, False otherwise
        """
        return all([bike.is_finished for bike in self.bikes])
