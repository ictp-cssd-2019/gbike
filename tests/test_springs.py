import sys

sys.path.append('..')
from springs import Spring
from road import Road
from vertex import Vertex
import numpy as np


def get_still_vertex(pos):
    vertex = Vertex(1, 1, pos, [0, 0], [0, 0], Road.get_flat_road(10))
    return vertex


def get_spring(pos1, pos2):
    return Spring(1, get_still_vertex(pos1), get_still_vertex(pos2))


def test_constructor():
    v1 = get_still_vertex([1, 0])
    v2 = get_still_vertex([0, 1])
    spring = Spring(1., v1, v2)
    assert isinstance(spring, Spring)


def test_distance():
    spring = get_spring([3, 0], [5, 0.5])
    assert np.isclose(spring.distance, np.sqrt(4.25))


def test_equilibrium_force():
    spring = get_spring([3, 0], [5, 0.5])
    force = spring.get_force()
    assert np.isclose(force, 0)  # Todo: Make a force test here, so we know what to expect


def test_push_force():
    spring = get_spring([0, 0], [5, 0])
    v2 = spring.vertices[1]
    v2.position = [2.5, 0]
    force = spring.get_force()
    assert np.isclose(force, 2.5)


def test_pull_force():
    spring = get_spring([0, 0], [5, 0])
    v2 = spring.vertices[1]
    v2.position = [10, 0]
    force = spring.get_force()
    assert np.isclose(force, -5)  #


def test_straight_angle():
    spring = get_spring([0, 0], [5, 0])
    v2 = spring.vertices[1]
    v2.position = [10, 0]
    angle = spring.get_angle(v2)
    assert np.isclose(np.cos(angle), 1)


def test_reverse_angle():
    spring = get_spring([0, 0], [5, 0])
    v2 = spring.vertices[1]
    v2.position = [10, 0]
    v1 = spring.vertices[0]
    angle = spring.get_angle(v1)
    assert np.isclose(np.cos(angle), -1)


def test_slanted_angle():
    spring = get_spring([2, 1.], [3., 2.])
    v2 = spring.vertices[1]
    v2.position = [4, 3.]
    angle = spring.get_angle(v2)
    assert np.isclose(angle, np.pi / 4)
