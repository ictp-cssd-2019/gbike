import sys

sys.path.append('..')
from springs import Spring
from road import Road
from vertex import Vertex
import numpy as np


def get_still_vertex(pos):
    vertex = Vertex(1, 1, pos, [0, 0], [0, 0], Road.get_flat_road(10))
    return vertex


def get_spring(pos1, pos2):
    return Spring(1, get_still_vertex(pos1), get_still_vertex(pos2))


# def test_complicated_spring_force_components():
#     v1 = get_still_vertex([0, 0])
#     v2 = get_still_vertex([1, 1])
#     v3 = get_still_vertex([-1,1])
#     spring1 = Spring(1., v1, v2)
#     spring2 = Spring(1., v1, v3)
#     v1.position = [1,0]
#     v1.connected_springs = [spring1,spring2]
#
#     forces = v1._compute_total_spring_force()
#     analytical_forces = [np.sqrt(2)/2,-np.sqrt(2)/2]
#     assert np.isclose(forces[0],analytical_forces[0])
#     assert np.isclose(forces[1],analytical_forces[1])

def test_simple_spring_force_components():
    v1 = get_still_vertex([0, 0])
    v2 = get_still_vertex([1, 1])
    spring1 = Spring(1., v1, v2)
    v1.position = [1, 0]
    v1.connected_springs = [spring1]

    forces = v1._compute_total_spring_force()
    analytical_forces = [0, -(np.sqrt(2) - 1)]
    assert np.isclose(forces[0], analytical_forces[0])
    assert np.isclose(forces[1], analytical_forces[1])
