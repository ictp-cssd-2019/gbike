import numpy as np
from vertex import Vertex, Wheel
from springs import Spring
from bike import Bike
import itertools
from road import Road


class BikeEvolver:
    """
    Class responsible for identifying the best bikes and creating virtual offspring for them
    """

    def __init__(self, road):
        """
        Creates a new evolver class
        """
        self.num_parents = 2
        self.road = road

    def get_new_generation(self, bikes):
        """
        Creates a new generation of bikes using a genetic algorithm that combines the features of the best bikes
        and adds some mutations

        :param bikes: List of bikes in the current generation that will be improved.
        :return: List of bikes from the new generation
        """
        best_bikes = self._select_best(bikes)
        best_genes = [Gene(bike) for bike in best_bikes]
        new_genes = [Gene.get_mixed_gene(best_genes[0], best_genes[1]) for _ in bikes]
        new_bikes = [Gene.get_bike(gene.gene_list, self.road) for gene in new_genes]
        return new_bikes

    def _select_best(self, bikes):
        """
        Select the `num_parents` best bikes based on the distance they travelled.

        :param bikes: List of bikes of the current generation
        :return: List of bikes which performed best
        """
        sorted_bikes = sorted(bikes, key=lambda b: b.distance_travelled, reverse=True)
        return sorted_bikes[:self.num_parents]


class Gene:
    """
    Provide a gene (list-like) representation of a bike used for the genetic algorithm
    The gene has the following properties
    The 6 spring lengths
    the 4 masses
    the 1 acceleration
    The location of the leading wheel
    The location of the second wheel
    :param bike: bike with numerical properties
    :return: gene (list of floats)
    """
    props = ["vertex0x", "vertex1x", "vertex2x", "vertex3x",
             "vertex0y", "vertex1y", "vertex2y", "vertex3y",
             "mass0", "mass1", "mass2", "mass3",
             "radius0", "radius1", "radius2", "radius3",
             "rot_acc_magnitude", "leading_wheel", "second_wheel"]
    prop_dict = {prop: float for prop in props}
    prop_dict['leading_wheel'] = int
    prop_dict['second_wheel'] = int
    mutation_probability = 0.2

    def __init__(self, bike):
        """
        Create a new gene based on bike properties

        :param bike: Bike to be converted to a gene
        """
        self.bike = bike
        self.road = bike.vertices[0].road
        num_prop = len(Gene.props)
        self.gene_list = {}
        self.fitness = bike.distance_travelled
        self.obtain_genes()

    def obtain_genes(self):
        num_vertices = 4
        for i in range(num_vertices):
            vertex = self.bike.vertices[i]
            if isinstance(vertex, Wheel):
                if vertex.rot_acc_magnitude > 0:
                    self.gene_list["leading_wheel"] = i
                    self.gene_list["rot_acc_magnitude"] = vertex.rot_acc_magnitude
                else:
                    self.gene_list["second_wheel"] = i
            self.gene_list["vertex%dx" % i] = vertex.position[0]
            self.gene_list["vertex%dy" % i] = vertex.position[1]
            self.gene_list["radius%d" % i] = vertex.radius
            self.gene_list["mass%d" % i] = vertex.mass

    @staticmethod
    def get_bike(gene_list, road):
        """
        Convert gene representation back to a bike

        :param gene_list: list of floats
        :param road: Road object.
        :return: new bike
        """
        vertices = []
        springs = []
        spring_dict = {}
        for i in range(4):
            if i == gene_list['leading_wheel']:
                vertex = Wheel(gene_list['rot_acc_magnitude'], gene_list['mass%d' % i], gene_list['radius%d' % i],
                               [gene_list['vertex%dx' % i], gene_list['vertex%dy' % i]],
                               velocity=[0, 0], acceleration=[0, 0], road=road)
            elif i == gene_list['second_wheel']:
                vertex = Wheel(0, gene_list['mass%d' % i], gene_list['radius%d' % i],
                               [gene_list['vertex%dx' % i], gene_list['vertex%dy' % i]],
                               velocity=[0, 0], acceleration=[0, 0], road=road)
            else:
                vertex = Vertex(gene_list['mass%d' % i], gene_list['radius%d' % i],
                            [gene_list['vertex%dx' % i], gene_list['vertex%dy' % i]],
                            velocity=[0, 0], acceleration=[0, 0], road=road)
            vertices.append(vertex)
        for v1, v2 in itertools.combinations(vertices, 2):
            spring = Spring(1., v1, v2)
            springs.append(spring)
            spring_dict[(v1, v2)] = spring
        name = ("%.3f" % np.random.random())[2:6]

        return Bike(name, vertices, springs, spring_dict)

    @staticmethod
    def get_mixed_gene(gene1, gene2):
        """
        Create new genes by recombining aspects of the current best bikes into new versions

        :param gene1: Best gene of the previous generation
        :param gene2: Second best gene of the previous generation
        :return: Stochastic repr of two genes
        """
        new_gene_list = {}
        for prop in Gene.props:
            if Gene.prop_dict[prop] == float:
                new_float = Gene.draw_float(gene1.gene_list[prop], gene2.gene_list[prop], gene1.fitness, gene2.fitness)
                new_float = Gene.mutate_float(new_float)
                new_gene_list[prop] = new_float
            elif Gene.prop_dict[prop] == int:
                new_int = Gene.draw_int(gene1.gene_list[prop], gene2.gene_list[prop], gene1.fitness, gene2.fitness)
                new_int = Gene.mutate_int(new_int)
                new_gene_list[prop] = new_int
        while new_gene_list['leading_wheel'] == new_gene_list['second_wheel']:
            new_gene_list['second_wheel'] = np.random.choice(Gene.props['second_wheel'])
        bike = Gene.get_bike(new_gene_list, gene1.road)
        return Gene(bike)

    @staticmethod
    def get_random_gene(road):
        bounds = {"vertex": [1, 3],
                  "mass": [1, 5],
                  "radius": [0.2, 3],
                  "rot_acc": [0.1, 60],
                  "leading_wheel": (0, 1, 2, 3),
                  "second_wheel": (0, 1, 2, 3)}
        new_gene_list = {}
        for prop in Gene.props:
            for reduced_prop in bounds:
                if prop.startswith(reduced_prop):
                    if Gene.prop_dict[prop] == float:
                        lower, upper = bounds[reduced_prop]
                        rand_float = np.random.random() * (upper - lower) + lower
                        new_gene_list[prop] = rand_float
                    elif Gene.prop_dict[prop] == int:
                        num = np.random.choice(bounds[reduced_prop])
                        new_gene_list[prop] = num
                        new_gene_list[prop] = num
        while new_gene_list['leading_wheel'] == new_gene_list['second_wheel']:
            new_gene_list['second_wheel'] = np.random.choice(bounds['second_wheel'])
        bike = Gene.get_bike(new_gene_list, road)
        return Gene(bike)

    @staticmethod
    def draw_float(gene1_val, gene2_val, gene1_fitness, gene2_fitness):
        """
        Using a pdf defined as
        0 if x < lower

        0 if x > upper
        :param gene1_val:
        :param gene2_val:
        :param gene1_fitness:
        :param gene2_fitness:
        :return:
        """
        u = np.sqrt(np.random.random())
        if gene1_fitness > gene2_fitness:
            return gene1_val * u + gene2_val * (1 - u)
        else:
            return gene1_val * (1 - u) + gene2_val * u

    @staticmethod
    def mutate_float(float_val):
        if np.random.random() < Gene.mutation_probability:
            return (np.random.random() + 0.5) * float_val
        else:
            return float_val

    @staticmethod
    def draw_int(gene1_val, gene2_val, gene1_fitness, gene2_fitness):
        prob1 = gene1_fitness / (gene1_fitness + gene2_fitness + 1E-4)
        if np.random.random() < prob1:
            return gene1_val
        else:
            return gene2_val

    @staticmethod
    def mutate_int(int_val):
        if np.random.random() < Gene.mutation_probability:
            return np.random.choice([0, 1, 2, 3])
        else:
            return int_val
