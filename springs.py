import numpy as np


class Spring:
    """
    Class that represents the spring: an object that delivers the force between two vertices.
    """

    def __init__(self, elastic_constant, vertex1, vertex2):
        """
        Create a new spring class
        The equilibrium length of the string is equal to the distance between its connecting vertices

        :param elastic_constant: Spring's modulus of elasticity (k constant)
        """
        vertex1.add_spring(self)
        vertex2.add_spring(self)
        self.vertices = (vertex1, vertex2)
        self.elastic_constant = elastic_constant
        self.equilibrium_distance = self.distance

    @property
    def distance(self):
        v1, v2 = self.vertices
        return np.sqrt((v2.position[0] - v1.position[0]) ** 2 + (v2.position[1] - v1.position[1]) ** 2)

    def get_force(self):
        """
        Calculate the spring_force F of the harmonic oscillator with a mass m pulled the direction of the point x = 0
        .. math::

            F=ma=m\\frac{\\mathrm{d}^{2}x }{\\mathrm{d} t^{^{2}}}=-kx

        :return: force of the spring as a float
        """
        spring_force = (self.distance - self.equilibrium_distance) * -self.elastic_constant
        return spring_force

    def get_angle(self, vertex):
        """
        Calculates the angle at which the spring force is delivered.

        :return: the direction of the spring (in radians)
        """
        v1, v2 = self.vertices
        if vertex == v1:
            other_vertex = v2
        elif vertex == v2:
            other_vertex = v1
        else:
            raise ValueError("Vertex %s not connected to spring %s" % (vertex, self))
        spring_angle = np.arctan2(vertex.position[1] - other_vertex.position[1],
                                  vertex.position[0] - other_vertex.position[0])
        return spring_angle

    def __str__(self):
        return "Spring: Equilibrium distance: %.2f, distance: %.2f" % (self.equilibrium_distance, self.distance)
