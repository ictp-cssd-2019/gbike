import numpy as np


class Vertex:
    """
    Class that represents a vertex: a mass point that is steered by the springs and kinematic
    motion to move the bike forward.
    """

    def __init__(self, mass, radius, position, velocity, acceleration, road):
        """
        Create a new vertex. The vertices are connected by springs and represent rigid bodies like wheels or loads.
        
        :param mass: Mass (kg) of the body
        :param radius: Radius (m) of the body
        :param position: A pair of values (m, m) representing the position of the body in the plane
        :param velocity: Linear velocity (m/s) of the body
        :param acceleration: Linear acceleration (m/s**2) of the body
        :param road: A road object that defines the slopes of the surface
        
        """
        self.gravity = 9.81
        self.mass = mass
        self.radius = radius
        self.position = np.array(position, dtype=float)
        self.velocity = np.array(velocity, dtype=float)
        self.acceleration = np.array(acceleration, dtype=float)
        self.road = road
        self.connected_springs = []
        self.touch = False

    def add_spring(self, spring):
        """
        Connect the given list of spring objects into the vertex

        :param spring: Spring connected to the vertex
        """
        self.connected_springs.append(spring)
        return

    def road_touch(self):
        """
        Determine whether the loads vertices touched the road.
        When a load touch the road, the bicycle travelling is finished.

        :return: True if the load has touched the ground, false otherwise
        """
        leading_point, rear_point = self._neighbor_points()
        nparr_position = np.array(self.position)
        center_to_road_distance = np.linalg.norm(
            np.cross(leading_point - rear_point, rear_point - nparr_position)) / np.linalg.norm(
            leading_point - rear_point)

        return self.radius >= center_to_road_distance

    def _neighbor_points(self):
        """
        Find the two nearest points of the road to the vertex position. # Todo: needs updating for random roads.

        :return: two x coordinates of the leading coordinates
        """
        transposed_road_points = np.transpose(self.road.road_points)

        leading_points = transposed_road_points[self.position[0] > self.road.road_points[0]]
        rear_points = transposed_road_points[self.position[0] < self.road.road_points[0]]

        leading_point = np.array(leading_points[0])  # first leading point of the road
        rear_point = np.array(rear_points[-1])  # last rear point of the road
        return leading_point, rear_point

    def compute_normal_force(self, spring_total_force):
        """
        Compute the normal force (N) affecting the body. # Fixme: This is not applied currently
        The normal force N is perpendicular and opposite to the gravity force:

        .. math::

            N=m.g.\\cos(\Theta)
        
        :param spring_total_force: the total force (N) coming from the springs.
        :return: normal force (N)
        """
        leading_point, rear_point = self._neighbor_points()
        slope_angle = np.arctan2(leading_point[1] - rear_point[1], leading_point[0] - rear_point[0])

        spring_total_force_angle = np.arctan(spring_total_force[1] / spring_total_force[0])
        spring_total_force_norm = np.linalg.norm(spring_total_force)
        normal_force = - self.gravity * self.mass * np.cos(slope_angle) + spring_total_force_norm * np.sin(
            slope_angle - spring_total_force_angle)
        return normal_force

    # def compute_spring_force(self, spring):
    #     """
    #     For a given spring will compute the force which gives to the vertex
    #     """
    #     return

    def _compute_total_spring_force(self):
        """
        Calculate the total force coming from the springs connected to the vertex
        
        .. math::

            F_{total}=\\sum_{i}^{n=3}-kx

        :return: The total force coming from both springs to the surface
        """
        spring_forces = np.zeros((len(self.connected_springs), 2))
        for i, spring in enumerate(self.connected_springs):
            spring_forces[i, 0] = spring.get_force() * np.cos(spring.get_angle(self))
            spring_forces[i, 1] = spring.get_force() * np.sin(spring.get_angle(self))

        spring_total_force = np.sum(spring_forces, axis=0)
        return spring_total_force

    def update_vertex(self, dt):
        """
        Compute the next position of the vertex from current position and conditions under Newton's laws of motion.

        .. math:
        
            F=m\\frac{\\mathrm{d}^{2}x}{\\mathrm{d} t^{2}}
        
        :param dt: Time step (s)
        """
        spring_total_force = self._compute_total_spring_force()
        self.acceleration = spring_total_force / self.mass

        if self.road_touch():
            self.acceleration[1] = 0  # Todo: This is a forced value
            self.velocity[1] = 0  # Todo: This is a forced value
        else:
            self.acceleration[1] -= self.gravity
        self._push_vertex(dt)

    def _push_vertex(self, dt):
        """
        Apply Euler algorithm to update the position of the vertex using the calculated acceleration, velocity
        and current position.

        :param dt: Time step size (s)
        """
        self.velocity += dt * self.acceleration
        self.position += dt * self.velocity  # Approximation of higher order exist.

    def __str__(self):
        return "Load: Mass: %.2f, Radius: %.2f, Position: (%.2f,%.2f)" % \
               (self.mass, self.radius, self.position[0], self.position[1])


class Wheel(Vertex):
    """
    Class that models a wheel: a vertex that has a positive radial acceleration (1/s**2).
    """

    def __init__(self, rotational_acceleration, *args, **kwargs):
        """
        Create a new wheel. This wheel is subclassed from Vertex but differs
        in that it has a rotational acceleration.

        :param rotational_acceleration: Positive rotational acceleration that leads the bike if positive
        :param args: Vertex unnamed arguments
        :param kwargs: Vertex named arguments
        """
        super().__init__(*args, **kwargs)
        self.rot_acc_magnitude = rotational_acceleration

    def update_vertex(self, dt):
        """
        Compute the next position of the vertex from current position and conditions under Newton's laws of motion.

        .. math:

            F=m\\frac{\\mathrm{d}^{2}x}{\\mathrm{d} t^{2}}

        :param dt: Time step (s)
        """
        spring_total_force = self._compute_total_spring_force()
        self.acceleration = spring_total_force / self.mass

        if self.road_touch():
            self.acceleration[0] += self.radius * self.rot_acc_magnitude
            self.acceleration[1] = 0  # Todo: This is a forced value
            self.velocity[1] = 0  # Todo: This is a forced value
        else:
            self.acceleration[1] -= self.gravity
        self._push_vertex(dt)

    def __str__(self):
        return "Wheel: Mass: %.2f, Radius: %.2f, Position: (%.2f,%.2f), Leading: %s" % \
               (self.mass, self.radius, self.position[0], self.position[1], self.rot_acc_magnitude > 0)
